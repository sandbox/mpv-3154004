<?php

namespace Drupal\rest_password_reset\ParamConverter;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\ParamConverter\EntityConverter;
use Drupal\Core\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\ContextDefinition;

/**
 * Parameter converter for upcasting entity IDs to full objects.
 *
 * This is useful in cases where the dynamic elements of the path can't be
 * auto-determined; for example, if your path refers to multiple of the same
 * type of entity ("example/{node1}/foo/{node2}") or if the path can act on any
 * entity type ("example/{entity_type}/{entity}/foo").
 *
 * In order to use it you should specify some additional options in your route:
 * @code
 * example.route:
 *   path: foo/{example}
 *   options:
 *     parameters:
 *       example:
 *         type: entity:node
 * @endcode
 *
 * If you want to have the entity type itself dynamic in the url you can
 * specify it like the following:
 * @code
 * example.route:
 *   path: foo/{entity_type}/{example}
 *   options:
 *     parameters:
 *       example:
 *         type: entity:{entity_type}
 * @endcode
 *
 * If your route needs to support pending revisions, you can specify the
 * "load_latest_revision" parameter. This will ensure that the latest revision
 * is returned, even if it is not the default one:
 * @code
 * example.route:
 *   path: foo/{example}
 *   options:
 *     parameters:
 *       example:
 *         type: entity:node
 *         load_latest_revision: TRUE
 * @endcode
 *
 * When dealing with translatable entities, the "load_latest_revision" flag will
 * make this converter load the latest revision affecting the translation
 * matching the content language for the current request. If none can be found
 * it will fall back to the latest revision. For instance, if an entity has an
 * English default revision (revision 1) and an Italian pending revision
 * (revision 2), "/foo/1" will return the former, while "/it/foo/1" will return
 * the latter.
 *
 * @see entities_revisions_translations
 */
class RestPasswordResetEntityConverter extends EntityConverter {
  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults) {
    $entity_type_id = $this->getEntityTypeFromDefaults($definition, $name, $defaults);

    // If the entity type is revisionable and the parameter has the
    // "load_latest_revision" flag, load the active variant.
    if (!empty($definition['load_latest_revision'])) {
      return $this->entityRepository->getActive($entity_type_id, $value);
    }

    // Do not inject the context repository as it is not an actual dependency:
    // it will be removed once both the TODOs below are fixed.
    /** @var \Drupal\Core\Plugin\Context\ContextRepositoryInterface $contexts_repository */
    $contexts_repository = \Drupal::service('context.repository');
    // @todo Consider deprecating the legacy context operation altogether in
    //   https://www.drupal.org/node/3031124.
    $contexts = $contexts_repository->getAvailableContexts();
    $contexts[EntityRepositoryInterface::CONTEXT_ID_LEGACY_CONTEXT_OPERATION] =
      new Context(new ContextDefinition('string'), 'entity_upcast');
    // @todo At the moment we do not need the current user context, which is
    //   triggering some test failures. We can remove these lines once
    //   https://www.drupal.org/node/2934192 is fixed.
    $context_id = '@user.current_user_context:current_user';
    if (isset($contexts[$context_id])) {
      $account = $contexts[$context_id]->getContextValue();

      $user_pass_reset = FALSE;
      if (($token = \Drupal::request()->get('pass-reset-token')) && \Drupal::request()->get('_format')) {
        $session_key = 'pass_reset_' . $account->id();
        $user_pass_reset = isset($_SESSION[$session_key]) && Crypt::hashEquals($_SESSION[$session_key], $token);
      }

      if (!$user_pass_reset) {
        unset($account->_skipProtectedUserFieldConstraint);
        unset($contexts[$context_id]);
      }
    }
    $entity = $this->entityRepository->getCanonical($entity_type_id, $value, $contexts);

    return $entity;
  }

}
